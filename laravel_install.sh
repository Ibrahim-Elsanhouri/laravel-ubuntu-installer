#!/bin/bash
################################################################################
# Script for installing Odoo on Ubuntu 16.04, 18.04 and 20.04 (could be used for other version too)
# Author: Yenthe Van Ginneken
#-------------------------------------------------------------------------------
# This script will install Odoo on your Ubuntu 16.04 server. It can install multiple Odoo instances
# in one Ubuntu because of the different xmlrpc_ports
#-------------------------------------------------------------------------------
# Make a new file:
# sudo nano odoo-install.sh
# Place this content in it and then make the file executable:
# sudo chmod +x odoo-install.sh
# Execute the script to install Odoo:
# ./odoo-install
################################################################################

# The default port where this Odoo instance will run under (provided you use the command -c in the terminal)
# Set to true if you want to install it, false if you don't need it or have it already installed.
#INSTALL_WKHTMLTOPDF="True"
# Set the default Odoo port (you still have to use -c /etc/odoo-server.conf for example to use this.)
#OE_PORT="8069"
# Choose the Odoo version which you want to install. For example: 13.0, 12.0, 11.0 or saas-18. When using 'master' the master version will be installed.
# IMPORTANT! This script contains extra libraries that are specifically needed for Odoo 13.0
#OE_VERSION="15.0"
# Set this to True if you want to install the Odoo enterprise version!
#IS_ENTERPRISE="False"
# Set this to True if you want to install Nginx!
INSTALL_NGINX="True"
# Set the superadmin password - if GENERATE_RANDOM_PASSWORD is set to "True" we will automatically generate a random password, otherwise we use this one
# Set to "True" to generate a random password, "False" to use the variable in OE_SUPERADMIN
#GENERATE_RANDOM_PASSWORD="True"
#OE_CONFIG="${OE_USER}-server"
# Set the website name
WEBSITE_NAME="udemy"
# Set the default Odoo longpolling port (you still have to use -c /etc/odoo-server.conf for example to use this.)
#LONGPOLLING_PORT="8072"
# Set to "True" to install certbot and have ssl enabled, "False" to use http
ENABLE_SSL="True"
# Provide Email to register ssl certificate
ADMIN_EMAIL="odoo@example.com"
##
###  WKHTMLTOPDF download links
## === Ubuntu Trusty x64 & x32 === (for other distributions please replace these two links,
## in order to have correct version of wkhtmltopdf installed, for a danger note refer to
## https://github.com/odoo/odoo/wiki/Wkhtmltopdf ):
## https://www.odoo.com/documentation/13.0/setup/install.html#debian-ubuntu

#WKHTMLTOX_X64="https://github.com/wkhtmltopdf/wkhtmltopdf/releases/download/0.12.5/wkhtmltox_0.12.5-1.$(lsb_release -c -s)_amd64.deb"
#WKHTMLTOX_X32="https://github.com/wkhtmltopdf/wkhtmltopdf/releases/download/0.12.5/wkhtmltox_0.12.5-1.$(lsb_release -c -s)_i386.deb"
#--------------------------------------------------
# Update Server
#--------------------------------------------------
echo -e "\n---- Update Server ----"
# universe package is for Ubuntu 18.x
sudo apt-get update

#--------------------------------------------------
# Install PHP Dependinces
#--------------------------------------------------
echo -e "\n---- Installing PHP ----"
# universe package is for Ubuntu 18.x
sudo apt install php php-mbstring php-xml php-bcmath php-fpm php-mysql -y
sudo apt-get install php-curl -y 
#--------------------------------------------------
# Install PHP Dependinces
#--------------------------------------------------
echo -e "\n---- Purge Default Apache Installtion ----"
# universe package is for Ubuntu 18.x
sudo apt purge apache2 -y 
#--------------------------------------------------
#--------------------------------------------------
# Install Composer 
#--------------------------------------------------
echo -e "\n---- Install Composer ----"
sudo apt install composer -y
#--------------------------------------------------

# Install Composer 
#--------------------------------------------------
echo -e "\n---- Install Mysql ----"
sudo apt install mysql-server -y
#--------------------------------------------------




#edit the root

#--------------------------------------------------
# Install Nginx if needed
#--------------------------------------------------
if [ $INSTALL_NGINX = "True" ]; then
  echo -e "\n---- Installing and setting up Nginx ----"
  sudo apt install nginx -y
 

  echo "Done! The Nginx server is up and running. "
else
  echo "Nginx isn't installed due to choice of the user!"
fi

#--------------------------------------------------
# Enable ssl with certbot
#--------------------------------------------------

  sudo add-apt-repository ppa:certbot/certbot -y && sudo apt-get update -y
  sudo apt-get install python3-certbot-nginx -y
  sudo certbot --nginx -d $WEBSITE_NAME --noninteractive --agree-tos --email $ADMIN_EMAIL --redirect
  sudo service nginx reload
  echo "SSL/HTTPS is enabled!"
else
  echo "SSL/HTTPS isn't enabled due to choice of the user or because of a misconfiguration!"
fi
